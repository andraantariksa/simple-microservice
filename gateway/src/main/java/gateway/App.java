/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package gateway;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.port;

import net.dongliu.requests.Requests;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;

class Users {
    private int id;
    private String password;
    private String username;
    private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

class ResultResponse <T> {
    private boolean success;
    private String error;
    private T data;

    ResultResponse() {
        this.success = true;
    }

    public void setData(T data) {
        this.data = data;
    }

    public T getData() {
        return this.data;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}

class UserApiRequests {
    private final String base;
    private final Gson gson;

    UserApiRequests() {
        base = "http://localhost:" + System.getenv("SERVICE_USERS_PORT") + "/api/v1/";
        this.gson = new Gson();
    }

    private String post(String endpoint, HashMap bodyData) {
        return Requests.post(base + endpoint).body(bodyData).send().readToText();
    }

    private String get(String endpoint) {
        return Requests.get(base + endpoint).send().readToText();
    }

    public Users my(String username) {
        ResultResponse<Users> resultResponse = (ResultResponse<Users>) gson.fromJson(this.get("profile/" + username), new TypeToken<ResultResponse<Users>>(){}.getType());
        Users users = resultResponse.getData();
        return users;
    }

    public boolean register(HashMap registerForm) {
        ResultResponse<Users> resultResponse = gson.fromJson(this.post("register", registerForm), ResultResponse.class);
        return resultResponse.isSuccess();
    }
    
    public boolean isCredentialMatch(String username, String password) {
        ResultResponse<Users> resultResponse = (ResultResponse<Users>) gson.fromJson(this.get("profile/" + username), new TypeToken<ResultResponse<Users>>(){}.getType());
        Users users = resultResponse.getData();
        return users.getPassword().equals(password);
    }
}

public class App {
    public static void main(String[] args) {
        port(Integer.parseInt(System.getenv("SERVICE_GATEWAY_PORT")));

        Gson gson = new Gson();

        get("/ping", (req, res) -> "Pong");

        post("/register", (req, res) -> {
            HashMap registerForm = new HashMap<String, String>();

            registerForm.put("username", req.queryParams("username"));
            registerForm.put("password", req.queryParams("password"));
            registerForm.put("email", req.queryParams("email"));

            UserApiRequests userApiRequests = new UserApiRequests();

            return userApiRequests.register(registerForm);
        });

        post("/login", (req, res) -> {
            String username = req.queryParams("username");
            String password = req.queryParams("password");

            UserApiRequests userApiRequests = new UserApiRequests();

            boolean proceed;
            
            try {
                proceed = userApiRequests.isCredentialMatch(username, password);
            } catch (Exception err) {
                proceed = false;
            }

            if (proceed) {
                req.session().attribute("username", username);
            }

            return proceed;
        });

        get("/my", (req, res) -> {
            String username = req.session().attribute("username");

            if (username == null) {
                return "You haven't login yet";
            }

            UserApiRequests userApiRequests = new UserApiRequests();
            
            return "Username: " + username + "<br />Email: " + userApiRequests.my(username).getEmail();
        });
    }
}
