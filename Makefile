export SERVICE_GATEWAY_PORT = 8080
export SERVICE_USERS_PORT = 8082
export POSTGRES_USERNAME = postgres
export POSTGRES_PASSWORD = 123
export POSTGRES_HOST = localhost
export POSTGRES_PORT = 5432
export POSTGRES_DATABASE = simple-microservice

migrate-db: clean-db
	cd commons && gradle flywayMigrate

create-db:
	createdb simple-microservice

clean-db:
	cd commons && gradle flywayClean

run-gateway:
	cd gateway && gradle run

run-users:
	cd users && gradle run