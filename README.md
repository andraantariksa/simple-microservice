# Simple Microservice

## Prerequisites

- OpenJDK 11
- Makefile
- Gradle
- Postgre

Note for Windows users: You have to run the program using [Cygwin](http://www.cygwin.com/)

## Getting Started

```
make run
```